function ClockFilter(form){
    Filter.apply(this, arguments);
}

ClockFilter.prototype = {
    getClockType : function() {
        this.resultObject.clockType = this.DOMElements.clockFilter.value;
    },
    getClockGender : function() {
        var elem = this.DOMElements.genderFilter; 
        for (var i = 0; i < elem.length; i++) {
            if (elem[i].checked) {                
                this.resultObject.gender = elem[i].value;
            }
        }        
    },
    readValues : function(e){
        e.preventDefault();        
        this.getPrice();
        this.getDate();
        this.getClockType();
        this.getClockGender();
        this.printValues();
    }
}

inheritense(Filter, ClockFilter);

var clockFilter = new ClockFilter({
    priceFilter  : document.querySelector("#priceFilter"),
	dateFilter 	 : document.querySelector("#dateFilter"),
    clockFilter  : document.querySelector("#typeFilter"),
    genderFilter : document.querySelectorAll(".gender"),
	resultText   : document.querySelector("#result-text"),
    submitBtn    : document.querySelector("#submit-btn"),
});

clockFilter.initListener();